<?php

namespace App\Storage;

use App\Storage\Contracts\TaskStorageInterface;
use App\Models\Task;
use PDO;
use DateTime;

class MySqlDatabaseTaskStorage implements TaskStorageInterface
{
	// protected $task;
	protected $id;

	public function __construct($pdo) {
		$this->pdo = $pdo;
	}

	public function store(Task $task)
	{
		$insert = $this->pdo->prepare('INSERT INTO tasks (description, due, complete) VALUES (:description, :due, :complete);');
		$insert->execute(array(
			':description' => $task->getDescription(),
			':due' => $task->getDue(),
			':complete' => $task->getComplete()
		));

		return $this->pdo->lastInsertId();
	}

	public function update(Task $task)
	{
		$update = $this->pdo->prepare('UPDATE tasks SET
			description = :description,
			due = :due, 
			complete = :complete
			WHERE id = :id;'
		);

		$update->execute(array(
			':id' => $task->getId(),
			':description' => $task->getDescription(),
			':due' => $task->getDue(),
			':complete' => $task->getComplete()
		));
	}

	public function get($id)
	{
	    $query = $this->pdo->query('SELECT * FROM tasks WHERE id = ' . $id);
	    $query->setFetchMode(PDO::FETCH_CLASS, Task::class);
	    $task = $query->fetch();
		return $task;
	}

	public function all()
	{
		$query = $this->pdo->query('SELECT * FROM tasks');
		$tasks = $query->fetchAll(PDO::FETCH_OBJ);

		foreach ($tasks as $t) {
		    $query = $this->pdo->query('SELECT * FROM tasks WHERE id = ' . $t->id);
		    $query->setFetchMode(PDO::FETCH_CLASS, Task::class);
		    $task = $query->fetch();
		    print_r($task);
		}
	}
}
