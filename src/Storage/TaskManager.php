<?php

namespace App\Storage;

use App\Storage\Contracts\TaskStorageInterface;
use App\Models\Task;

class TaskManager
{
	protected $client;

    public function __construct($storage)
    {
        $this->client = $storage;
    }

    public function addTask($task)
    {
        return $this->client->store($task);
    }

    public function getTask($task)
    {
        return $this->client->get($task);
    }

    public function updateTask($task)
    {
        return $this->client->update($task);
    }

    public function getTasks()
    {
        return $this->client->all();
    }
}
