<?php

namespace App\Models;

use DateTime;

class Task
{
    protected $id;
    protected $complete;
    protected $description;
    protected $due;

    public function getId()
    {
        return $this->id;
    }

    public function setId()
    {
        $this->id = $id;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function setDescription($description)
    {
        $this->description = $description;
    }

    public function getComplete()
    {
        return $this->complete;
    }

    public function setComplete($complete=1)
    {
        $this->complete = $complete;
    }

    public function getDue()
    {
        $datetime = new \DateTime($this->due);
        return $datetime->format('Y-m-d H:i:s');
    }

    public function setDue($due)
    {
        $this->due = $due;
    }
}