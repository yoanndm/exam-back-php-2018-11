<?php

require __DIR__ . '/../vendor/autoload.php';

$dotenv = new Dotenv\Dotenv(__DIR__ . '/../');
$dotenv->load();

use App\Models\Task;
use App\Storage\MySqlDatabaseTaskStorage;
use App\Storage\TaskManager;

try {
	// Successful database connection
	$pdo = new PDO('mysql:host='.$_ENV['DB_HOST_LOCAL'].';dbname='.$_ENV['DB_NAME'], $_ENV['DB_USER_LOCAL'], $_ENV['DB_PSSW_LOCAL']);
	$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING);
} catch(Exception $e) {
	// Database connection failed
	throw new Exception($e->getMessage());
}

#####
# 2 #
#####
// $task = new Task;

// $query = $pdo->query('SELECT * FROM tasks');
// $tasks = $query->fetchAll(PDO::FETCH_OBJ);

// foreach ($tasks as $t) {
//     $query = $pdo->query('SELECT * FROM tasks WHERE id = ' . $t->id);
//     $query->setFetchMode(PDO::FETCH_CLASS, Task::class);
//     $task = $query->fetch();
//     echo $task->getDescription() . ' - ';
//     echo $task->getDue() . '</br>';
// }

#####
# 4 #
#####
// // Get all tasks
// $storage = new MySqlDatabaseTaskStorage($pdo);
// // print_r($storage->all());

// // Create a task
// $task = new Task;
// $task->setDescription('Go to Webstart');
// $task->setDue('+ 3 days');

// $taskId = $storage->store($task);
// print_r($storage->get($taskId));

// $task = $storage->get(18);
// $task->setComplete(1);
// $storage->update($task);

#####
# 5 #
#####

$storage = new MySqlDatabaseTaskStorage($pdo);
$manager = new TaskManager($storage);

// Create a task
$task = new Task;
$task->setDescription('A new task');
$task->setDue('');
$addedTask = $manager->addTask($task);

// Update (and get)
$task = $manager->getTask(5);
$task->setComplete();
$manager->updateTask($task);

// Tasks
$tasks = $manager->getTasks();
print_r($tasks);